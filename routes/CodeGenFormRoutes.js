const express = require('express');
const router = express.Router();
const form = require('../controller/CodeGenFormController'); 
router.post('/getData', form.loadFormData);
router.post('/outputFileUpdate', form.outputFileUpdate);
router.post('/submitForm', form.SubmitInfo);
router.get('/', form.fetchFormData); 

module.exports = router;